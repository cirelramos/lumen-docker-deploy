#!/bin/bash
    echo "LOG MYSQL actived"
    cp /opt/config-mysql/mysql.cnf  /etc/mysql/conf.d/mysql.cnf
    cp /opt/config-mysql/docker.cnf  /etc/mysql/conf.d/docker.cnf
    echo "" > /var/log/mysql/query.log
    echo "" > /var/log/mysql/slow.log
    chmod 777 /var/log/mysql/slow.log
    chmod 777 /var/log/mysql/query.log
    chown -R mysql:mysql /etc/mysql/
    chown -R mysql:mysql /var/log/mysql/
    chown -R mysql:mysql /etc/mysql/
    chown -R mysql:mysql /var/log/mysql/
