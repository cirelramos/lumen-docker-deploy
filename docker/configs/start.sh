#!/bin/bash
#setfacl -Rdm g:www-data:rwx /var/www/
#setfacl -Rdm o:www-data:rwx /var/www/
#setfacl -Rdm o::rwx /var/www/
echo $HRROLE
echo $TYPE_DEPLOY
case "$HRROLE" in
  "frontend")
    case "$TYPE_DEPLOY" in
      "local-deploy")
        bash -sic "whereis yarn"
        bash -sic "cd /var/www/backend/ && yarn install && yarn $TYPE_DEPLOY"
        ;;
      "local-deploy-after-install")
        bash -sic "whereis yarn"
        bash -sic "cd /var/www/backend/ && yarn $TYPE_DEPLOY"
        ;;
      "qa-deploy")
        bash -sic "cd /var/www/backend/ && rm -rf node_modules && rm -rf build && yarn install && yarn "$TYPE_DEPLOY" "
        ;;
      *)
        bash -sic "cd /var/www/backend/ && rm -rf node_modules && rm -rf build && yarn install && yarn start "
        ;;
    esac
    ;;
  "backend-service")
    bash -sic "chown -R www-data:www-data /var/www/"
    bash -sic "chown -R www-data:www-data /var/lib/nginx"
    bash -sic "chown -R www-data:www-data /var/lib/nginx && cd /var/www/backend/ && composer $TYPE_DEPLOY && nohup /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord-service.conf"
    ;;
  "backend-horizon")
    bash -sic "chown -R www-data:www-data /var/www/"
    bash -sic "chown -R www-data:www-data /var/lib/nginx"
    bash -sic "chown -R www-data:www-data /var/lib/nginx && crond && cd /var/www/backend/ && composer $TYPE_DEPLOY && nohup /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf"
    ;;
  *)
    echo "No especifico el HRROLE"
    ;;
esac
